# Database Tasks

### Database Design, Implementation, and Maintenance
Design and implement a database on top of the following description:

Travel Agency needs a system for online holidays booking. The system should store information about trips (e.g., date from, date to, destination, number of available trips, assigned excursions the clients could make on this trip), excursions (date and duration, description, destination, price), customers personal data, reservations - for each customer it should be stored, what trips with which excursions he has reserved. The customer can browse the trips catalogue, she/he can make reservations in which he can also select optional excursions. Administrator can use administrative interface for managing trips and excursions (CRUD) and also an ability to list all customers and reservations. Take into account that both customer and administrator can sign-in to the system.

**Design conditions:**
- For each table insert at least two records and for selected two tables 50 records.
- The database must be at least in the 3rd normal form (describe why the database is in 3rd normal form).
- At least three tables must have more than five columns.
- Database tables and columns should use consistent naming conventions (e.g., all table or
column names as lowercase letters; when the name is multi-word, separate it by "_" character).
- Database tables and columns must be named with English terms.
- Consider integrity constraints and describe why you choose such an integrity constraints.

**SQL conditions:**
- Create a query that will return selected columns from the selected table.
- Create a queries that will use the following keywords in a meaningful queries (you can combine them):
  - WHERE.
  - JOIN; LEFT JOIN; RIGHT JOIN.
  - DISTINCT; COUNT.
  - SUBSTRING; TRIM; CONCAT.
  - GROUP BY; HAVING; ORDER BY.
- If you doubt, additionally create any complicated query that will use any difficulties of your choice.
- Create a materialized view and describe the purpose of materialized views.
- Create a view and describe the purpose of views.
- Create a query that will return the data from an arbitrary table for the last one and half days (1day + 12 hours, i.e., 36 hours). Do not hard code the query (e.g., created_at > 7-11-2021)! Do it programmatically with DATE functions.
- Create a query for pagination in an application (use LIMIT and OFFSET).
- Create database indexes (create it only on columns where it might make sense). Describe why you choose such columns.

**Advanced Features**
- Create a script that will create a role for an application (e.g., my-app-role) that will have the following privileges SELECT, INSERT, UPDATE, and DELETE from arbitrary table in the selected database. This role will have a possibility to connect to the database (GRANT CONNECT...).
- Create a script to backup the database every midnight.
- Create a script to backup the database log files every hour.
- **Optional**: Encrypt selected columns in the database.

**As a result deliver the following (via link to the GitLab/GitHub repository):**
- DDL scripts to create a database (with `.sql` extension).
- DML scripts to insert a dummy data  (with `.sql` extension).
- Create a document (in .pdf) providing the following:
  - Description of each table in the database (the purpose for each table).
  - The evidence that you created a database with all the tables (e.g., screenshots from the pgAdmin tool).
  - All the SQL queries that you have made (optimally with screenshots from the query results).
- All the remaining scripts or work that you have done.
